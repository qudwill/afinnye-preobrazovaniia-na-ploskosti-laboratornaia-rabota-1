'use strict';


/**
 ** Процедура, реализующая формирование матрицы масштабирования
 */

var Scaling = (function() {
	var canvas = document.getElementById('scaling-canvas');
	var ctx = canvas.getContext('2d');
	var turnResult = $('#scaling-result');

	function setupListeners() {
		$('#scaling').on('submit', _submit);
	}

	function _insertMatrix(a_x, a_y, b_x, b_y, c_x, c_y, factor_x, factor_y) {
		var html = '';
		var aMatrix = _getMatrix('A', a_x, a_y, factor_x, factor_y);
		var bMatrix = _getMatrix('B', b_x, b_y, factor_x, factor_y);
		var cMatrix = _getMatrix('C', c_x, c_y, factor_x, factor_y);

		html += aMatrix + bMatrix + cMatrix;

		turnResult.html(html);

  	MathJax.Hub.Queue(['Typeset', MathJax.Hub]);
	}

	function _getMatrix(label, x, y, factor_x, factor_y) {
		var html = '<h3>Операция масштабирования в матричном виде для точек ' + label + ' и ' + label + '\'</h3>';

		html += '\\([x\', y\'] = [' + x + ', ' + y +'] *\\) $\\begin{bmatrix}' + factor_x +' & 0\\\\0 & ' + factor_y + '\\end{bmatrix}$';

		return html;
	}

	function _drawTriangles(a_x, a_y, b_x, b_y, c_x, c_y, factor_x, factor_y) {
		canvas.style.display = 'block';

		ctx.clearRect(0, 0, canvas.width, canvas.height);
		ctx.beginPath();
    ctx.moveTo(a_x + canvas.width / 2, canvas.height / 2 - a_y);
    ctx.lineTo(b_x + canvas.width / 2, canvas.height / 2 - b_y);
    ctx.lineTo(c_x + canvas.width / 2, canvas.height / 2 - c_y);
    ctx.lineTo(a_x + canvas.width / 2, canvas.height / 2 - a_y);
    ctx.moveTo((a_x * factor_x + canvas.width / 2), (canvas.height / 2 - a_y * factor_y));
    ctx.lineTo((b_x * factor_x + canvas.width / 2), (canvas.height / 2 - b_y * factor_y));
    ctx.lineTo((c_x * factor_x + canvas.width / 2), (canvas.height / 2 - c_y * factor_y));
    ctx.lineTo((a_x * factor_x + canvas.width / 2), (canvas.height / 2 - a_y * factor_y));
    ctx.stroke();
	}

	function _submit(e) {
		e.preventDefault();

		var $this = $(this);
		var data = $this.serializeArray();
		var a_x = parseInt(data[0].value);
		var a_y = parseInt(data[1].value);
		var b_x = parseInt(data[2].value);
		var b_y = parseInt(data[3].value);
		var c_x = parseInt(data[4].value);
		var c_y = parseInt(data[5].value);
		var factor_x = parseInt(data[6].value);
		var factor_y = parseInt(data[7].value);

		_insertMatrix(a_x, a_y, b_x, b_y, c_x, c_y, factor_x, factor_y);
		_drawTriangles(a_x, a_y, b_x, b_y, c_x, c_y, factor_x, factor_y);
	}

	return {
		init: function() {
			setupListeners();
		}
	}
}());