'use strict';


/**
 ** Процедура, реализующая формирование матрицы поворота
 */

var Turn = (function() {
	var canvas = document.getElementById('turn-canvas');
	var ctx = canvas.getContext('2d');
	var turnResult = $('#turn-result');

	function setupListeners() {
		$('#turn').on('submit', _submit);
	}

	function _getNewCoords(x, y, alpha) {
		var a = x * Math.cos(alpha) - y * Math.sin(alpha);
		var b = x * Math.sin(alpha) + y * Math.cos(alpha);

		return [a, b];
	}

	function _drawLineFromCoordStart(x, y, clear) {
		canvas.style.display = 'block';

		clear ? ctx.clearRect(0, 0, canvas.width, canvas.height) : '';

		ctx.beginPath();
    ctx.moveTo(canvas.width / 2, canvas.height / 2);
    ctx.lineTo(x + canvas.width / 2, canvas.height / 2 - y);
    ctx.stroke();
	}

	function _insertMatrix(x, y, alpha) {
		var html = '<h3>Матричный вид вращения точки [' + x + ', ' + y + '] на угол ' + alpha + '</h3>';

		html += '\\([x\', y\'] = [' + x + ', ' + y +'] *\\) $\\begin{bmatrix}' + Math.cos(alpha) +' & ' + Math.sin(alpha) + '\\\\' + -Math.sin(alpha) + ' & ' + Math.cos(alpha) + '\\end{bmatrix}$';

		turnResult.html(html);

  	MathJax.Hub.Queue(['Typeset', MathJax.Hub]);
	}

	function _submit(e) {
		e.preventDefault();

		var $this = $(this);
		var data = $this.serializeArray();
		var x = parseInt(data[0].value);
		var y = parseInt(data[1].value);
		var alpha = (parseInt(data[2].value) * Math.PI) / 180;
		var newCoords = _getNewCoords(x, y, alpha);
		var x_moved = newCoords[0];
		var y_moved = newCoords[1];

		_insertMatrix(x, y, alpha);
		_drawLineFromCoordStart(x, y, true);
		_drawLineFromCoordStart(x_moved, y_moved);
	}

	return {
		init: function() {
			setupListeners();
		}
	}
}());