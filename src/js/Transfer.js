'use strict';


/**
 ** Процедура, реализующая формирование матрицы переноса
 */

var Transfer = (function() {
	var transferResult = $('#transfer-result');

	function setupListeners() {
		$('#transfer').on('submit', _submit);
	}

	function _insertMatrix(x, y, x_moved, y_moved) {
		var html = '<h3>Векторная запись перехода из точки [' + x + ', ' + y + '] в точку [' + (x + x_moved) + ', ' + (y + y_moved) + ']</h3>';

		html += '\\(\\vec{B} = \\vec{A} + \\vec{R} = [' + x + ' + ' + x_moved + ', ' + y + ' + ' + y_moved + '] = [' + (x + x_moved) + ', ' + (y + y_moved) + ']\\)';

		transferResult.html(html);

  	MathJax.Hub.Queue(['Typeset', MathJax.Hub]);
	}

	function _submit(e) {
		e.preventDefault();

		var $this = $(this);
		var data = $this.serializeArray();
		var x = parseInt(data[0].value);
		var y = parseInt(data[1].value);
		var x_moved = parseInt(data[2].value);
		var y_moved = parseInt(data[3].value);

		_insertMatrix(x, y, x_moved, y_moved);
	}

	return {
		init: function() {
			setupListeners();
		}
	}
}());