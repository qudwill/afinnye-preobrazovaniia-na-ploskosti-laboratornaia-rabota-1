'use strict';

var ArrowSpeedometer = (function() {
	var canvas = document.getElementById('speedometer-canvas');
	var ctx = canvas.getContext('2d');
	var centerX = 150;
	var centerY = 200;
	var defaultArrowLength = 100;
	var arrowLength = defaultArrowLength;
	var interval;
	var $submitAcceleration = $('#submit-acceleration');

	function setupListeners() {
		$('#generate').on('click', _generateSpeedometer);
		$('#increase-size').on('click', _increaseSize);
		$('#decrease-size').on('click', _decreaseSize);
		$('#set-value').on('submit', _setSpeedValue);
		$('#set-acceleration').on('submit', _setAcceleration);
		$('#reset').on('click', _reset);
	}

	function _generateSpeedometer(e, x, y) {
		var xValue = x || 0;
		var yValue = y || 0;

		canvas.style.display = 'block';

		ctx.clearRect(0, 0, canvas.width, canvas.height);
		ctx.beginPath();

		_renderArrow(xValue, yValue, arrowLength);
		_renderNumbers(0 - arrowLength, 0, arrowLength);
	}

	function _renderArrow(x, y, length) {
		ctx.moveTo(centerX, centerY);
		ctx.lineTo(x + centerX - length, y + centerY);
		ctx.stroke();
	}

	function _renderNumbers(x, y, length) {
		var step = 20;

		ctx.textAlign = 'center';
		ctx.textBaseline = 'bottom';
		
		for (var i = 0; i <= 180; i += step) {
			ctx.fillText(i, x + centerX, y + centerY);

			var newCoords = _getNewCoords(x, y, (step * Math.PI / 180));

			x = newCoords[0];
			y = newCoords[1];
		}
	}

	function _getNewCoords(x, y, alpha) {
		var a = x * Math.cos(alpha) - y * Math.sin(alpha);
		var b = x * Math.sin(alpha) + y * Math.cos(alpha);

		return [a, b];
	}

	function _increaseSize() {
		arrowLength += 10;

		_generateSpeedometer();
	}

	function _decreaseSize() {
		arrowLength -= 10;

		_generateSpeedometer();
	}

	function _setSpeedValue(e, x, y) {
		e ? e.preventDefault() : '';

		var xValue = x || 0;
		var yValue = y || 0;
		var speedValue = parseInt($('#speed').val());
		var newCoords = _getNewCoords(xValue - arrowLength, yValue, (speedValue * Math.PI / 180));

		_generateSpeedometer(null, newCoords[0] + arrowLength, newCoords[1]);
	}

	function _setAcceleration(e) {
		e.preventDefault();

		var seconds = parseInt($('#seconds').val());
		var x = -arrowLength;
		var y = 0;

		$submitAcceleration.attr('disabled', true);

		interval = setInterval(function() {
			var newCoords = _getNewCoords(x, y, (2 / seconds * Math.PI / 180));

			x = newCoords[0];
			y = newCoords[1];

			_generateSpeedometer(null, x + arrowLength, y);

			if (x > 99) {
				_reset();

				return false;
			}
		}, 10);
	}

	function _reset() {
		arrowLength = defaultArrowLength;

		clearInterval(interval);
		_generateSpeedometer();

		$submitAcceleration.attr('disabled', false);
	}

	return {
		init: function() {
			setupListeners();
		}
	}
}());